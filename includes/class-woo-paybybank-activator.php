<?php

/**
 * Fired during plugin activation
 *
 * @link       https://studiorav.co.uk
 * @since      1.0.0
 *
 * @package    Woo_PaybyBank
 * @subpackage Woo_PaybyBank/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Woo_PaybyBank
 * @subpackage Woo_PaybyBank/includes
 * @author     Studiorav.co.uk
 */
class Woo_PaybyBank_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
