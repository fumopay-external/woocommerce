<?php
class WC_PaybyBank_Gateway extends WC_Payment_gateway
{
    /**
     * Class constructor, more about it in Step 3
     */
    public function __construct()
    {
        $this->id = 'paybybank-gateway'; // payment gateway plugin ID
        $this->icon = WOO_PAYBYBANK_PLUGIN_URL . 'assets/paybybank.png';
        $this->has_fields = true;
        $this->method_title = 'Pay by Bank | fumopay';
        $this->method_description = 'Pay by Bank is a secure account-to-account payment facilitated by fumopay. No card data entry, simply select your bank, authorise and pay. Powered by fumopay®';

        // gateways can support subscriptions, refunds, saved payment methods
        $this->supports = array(
            'products',
            'refunds'
        );
        $this->init_form_fields();

        $this->init_settings();
        $this->title = $this->get_option('title');
        $this->description = $this->get_option('description');
        $this->enabled = $this->get_option('enabled');
        $this->testmode = $this->get_option('testmode');

        $this->template_id = $this->get_option('template_id');

        if ($this->testmode != 'yes') {
            $this->apiURL = 'https://fumopay.app';
            $this->profile_key = $this->get_option('profile_key');
            $this->secret_key = $this->get_option('secret_key');
        } else {
            $this->apiURL = 'https://fumopay.dev';
            $this->profile_key = $this->get_option('test_profile_key');
            $this->secret_key = $this->get_option('test_secret_key');
        }
        // $this->description_for_customer_side = $this->get_option('description_for_customer_side');
        $this->order_button_text = __('Pay by Bank', 'woocommerce');

        add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));

        // webhook URL https://websiteurl.com/wc-api/paybybank-payment-response
        add_action('woocommerce_api_paybybank_payment_response', array($this, 'paybybank_webhook'));
        add_action('woocommerce_refund_created', array($this, 'redirect_to_auth_refund'), 10, 2);
        // add_action('woocommerce_create_refund', array($this, 'handle_failed_refund'), 10, 2);
        add_action('admin_notices', array($this, 'redirect_after_refund_notice'));

        add_action( 'save_post', array($this, 'save_order_bank_data_metabox'), 10, 2 );
        add_action( 'add_meta_boxes', array($this, 'save_customer_account_details_metabox') );

        $this->logger = wc_get_logger();
    }

    /**
     * Plugin options, we deal with it in Step 3 too
     */
    public function init_form_fields()
    {
        $this->form_fields = array(
            'enabled' => array(
                'title'       => 'Enable/Disable',
                'label'       => 'Enable Pay by Bank Gateway',
                'type'        => 'checkbox',
                'description' => '',
                'default'     => 'no'
            ),
            'title' => array(
                'title'       => 'Title',
                'type'        => 'text',
                'description' => 'This controls the title which the user sees during checkout.',
                'default'     => $this->method_title,
                'desc_tip'    => true,
            ),
            'description' => array(
                'title'       => 'Description',
                'type'        => 'textarea',
                'description' => 'This controls the description which the user sees during checkout.',
                'default'     => $this->method_description,
            ),
            'age' => array(
                'title'       => 'Transaction age',
                'type'        => 'number',
                'description' => 'This defines the age in hours for a payment to be completed otherwise it will be expired.',
                'default'     => 1,
                'desc_tip'    => true,
            ),
            'testmode' => array(
                'title'       => 'Test mode',
                'label'       => 'Enable Test Mode',
                'type'        => 'checkbox',
                'description' => 'Place the payment gateway in test mode using test API keys.',
                'default'     => 'yes',
                'desc_tip'    => true,
            ),
            'test_profile_key' => array(
                'title'       => 'Test Profile Key',
                'type'        => 'text'
            ),
            'test_secret_key' => array(
                'title'       => 'Test Secret Key',
                'type'        => 'password',
            ),
            'profile_key' => array(
                'title'       => 'Live Profile Key',
                'type'        => 'text'
            ),
            'secret_key' => array(
                'title'       => 'Live Secret Key',
                'type'        => 'password'
            )
        );
    }

    /**
     * You will need it if you want your custom credit card form, Step 4 is about it
     */
    public function payment_fields()
    {
        echo '<style>
        label[for="payment_method_paybybank-gateway"] img {
            max-width: 140px;
            margin-left: 10px;
        }
        
        label[for="payment_method_paybybank-gateway"] {
            display: flex;
            align-items: center;
        }</style>';

        if ($this->description) {
            if ($this->testmode == 'yes') {
                $this->description .= '<br><strong style="color:red">Pay by Bank is on test mode.</strong>';
            }
            echo wpautop(wp_kses_post($this->description));
        }
    }

    /*
    * Fields validation, more in Step 5
    */
    public function validate_fields()
    {
        if (empty($_POST['billing_first_name'])) {
            wc_add_notice('First name is required!', 'error');
            return false;
        }
        return true;
    }

    /*
    * We're processing the payments here, everything about it is in Step 5
    */
    public function process_payment($order_id)
    {
        global $woocommerce;

        // we need it to get any order detailes
        $order = wc_get_order($order_id);

        $profileKey = $this->profile_key;
        $secretKey = $this->secret_key;

        // Process Description
        $description = "Payment for Order #" . $order_id . ' - ' . site_url();

        $customer = $order->get_customer_id();
        $amount = intval(number_format($order->get_total() * 100, 0, '', '')); //Convert to minor units
        $reference = 'order_' . $order_id;

        $strToHash = implode('', [get_woocommerce_currency(), $description, $profileKey, $reference, $secretKey]);
        $hashed = hash("sha512", $strToHash, true);
        $signature = base64_encode($hashed);

        update_post_meta($order_id, 'signature', $signature);
        $thank_you_page = $order->get_checkout_order_received_url();
        $payload['headers'] = array('Content-Type: application/json');
        $payload['body'] = json_encode(array(
            'merchant_profile' => array('profile_key' => $profileKey),
            'currency' => get_woocommerce_currency(),
            'description' => $description,
            'ext_reference' => $reference,
            'ext_customer' => 'Customer Email: ' . $order->get_billing_email(),
            'amount' => $amount,
            'age' => $this->age, //How many hours the transaction stays active. This is an optional field with a default of 1 hour.
            'redirect_url' => $thank_you_page,
            'signature' => $signature,
            'email' => $order->get_billing_email(),
            'platform' => 'woocommerce'
        ));

        setcookie('paybybank_order_id', $order_id, time() + 3600, '/');

        $response = wp_remote_post($this->apiURL . '/transaction/pay/', $payload);
        wc_get_logger()->info('URL ' . $this->apiURL . '/transaction/pay/' . ' | Payload ' . json_encode($payload), array('source' => 'paybybank'));
        wc_get_logger()->info('Response from Pay by Bank' . json_encode($response['body']), array('source' => 'paybybank'));
        

        if (!is_wp_error($response)) {
            $body = json_decode($response['body']);

            // Response Codes: 1: Pending/OK, 2: Declined, 3: Error, 11: Paid, 12: Failed
            if ($body->result == '11') {
                // we received the payment
                $order->payment_complete();
                $order->reduce_order_stock();
                // some notes to customer (replace true with false to make it private)
                $order->add_order_note('Your order is paid. Thank you!', true);
                // Empty cart
                $woocommerce->cart->empty_cart();
                
                // Redirect to the thank you page
                return array(
                    'result' => 'success',
                    'redirect' => $this->get_return_url($order)
                );
            } elseif ($body->result == '1') { //Pending payment - Redirect customer to the pay screen
                $order->update_status('on-hold', 'Payment is pending with Pay by Bank. Order will get updated once payment is completed.');
                // Redirect to the payment page
                return array(
                    'result' => 'success',
                    'redirect' => $this->apiURL . '/process/' . $profileKey . '/' . $body->transaction_id
                );
            } else {
                wc_add_notice('Please try again.', 'error');
                return;
            }
        } else {
            $order->update_status('cancelled', 'There was a problem redirecting to the payment gateway. If this problem persists please contact Customer Services.');
            throw new Exception(__('We are currently experiencing problems trying to connect to this payment gateway. Sorry for the inconvenience.', 'woo-paybybank'));
            wc_add_notice('Connection error.', 'error');
            return;
        }
    }

    /*
    * In case you need a webhook, like PayPal IPN etc
    */
    public function paybybank_webhook()
    {
        $data = json_decode(file_get_contents('php://input'), true);

        wc_get_logger()->info('Pay by Bank Webhook POST: ' . json_encode($data), array('source' => 'paybybank'));

        if (!isset($data['reference'])) {
            return false;
        }

        $ref = $data['reference'];
        if (isset($data['transaction_id'])) {
            $order_id = str_replace('order_', '', $ref);
            $order = wc_get_order($order_id);

            if ($data['result'] == '11') {
                wc_get_logger()->info('Pay by Bank Webhook - Payment Completed, Order ID:' . $order_id, array('source' => 'paybybank'));
                $order->add_order_note('Payment successful via Pay by Bank - transaction id: ' . $data['transaction_id']);
                $order->payment_complete();
                update_post_meta($order_id, 'transaction_id', $data['transaction_id']);
                $order->set_transaction_id($data['transaction_id']);
                $transaction = $this->get_transaction($order_id, $data['transaction_id']);
                wc_get_logger()->info('Pay by Bank Webhook - Payment Completed, Transaction:' . $transaction, array('source' => 'paybybank'));

                $transaction = json_decode($transaction);
                if(!isset($transaction->transaction->account_number)){
                    update_post_meta($order_id, '_no_account', true);
                }

                $order->save();
                $order->reduce_order_stock();
                update_option('webhook_debug', $data);
            } elseif ($data['result'] == '3') {
                wc_get_logger()->info('Pay by Bank Webhook - Payment Error, Order ID:' . $order_id, array('source' => 'paybybank'));
                $order->update_status('cancelled', 'There was an error with the payment.');
            } elseif ($data['result'] == '12') {
                wc_get_logger()->info('Pay by Bank Webhook - Payment Failed, Order ID:' . $order_id, array('source' => 'paybybank'));
                $order->update_status('cancelled', 'Payment failed.');
            } else {
                wc_get_logger()->info('Pay by Bank Webhook - Payment unknown error, Order ID:' . $order_id, array('source' => 'paybybank'));
            }
        } elseif ($data['refund_id']) {
            $order_id = str_replace('REF-order_', '', $data['reference']);
            $order = wc_get_order($order_id);
            $order->add_order_note('Order refunded. Refund ID: ' . $data['refund_id']);

            // $order->update_status('wc-refunded', 'Order refunded. Refund ID: ' . $data['refund_id']);
            wc_get_logger()->info('Pay by Bank Refund Webhook - ' . json_encode($data), array('source' => 'paybybank'));
        }
    }

    public function process_refund($order_id, $refundAmount = null, $reason = '')
    {
        $order = new WC_Order($order_id);

        if (!$this->can_refund_order($order)) {
            wc_get_logger()->info('Refund Failed: No transaction ID', array('source' => 'paybybank'));
            return new WP_Error('error', __('Refund Failed: No transaction ID', 'woo-paybybank'));
        }

        try {
            $transaction_id = $order->get_transaction_id();
            $transaction = $this->get_transaction($order_id, $transaction_id);
            wc_get_logger()->info('Get Transaction: ' . $transaction_id . ' AND ' . $transaction, array('source' => 'paybybank'));
            $transaction = json_decode($transaction);

            $account_name = get_post_meta( $order_id, '_account_name', true );
            $account_number = get_post_meta( $order_id, '_account_number', true );
            $account_sortcode = get_post_meta( $order_id, '_account_sortcode', true );
            $bank_id = get_post_meta( $order_id, '_bank_id', true );
            if(!isset($transaction->transaction->account_number) && (empty($account_name) || empty($account_number) || empty($account_sortcode) || empty($bank_id))){
                return new WP_Error('error', __('Refund Failed: No customer account available to send the refund. Please fill the customer account details on the meta box and save the order before process the refund.', 'woo-paybybank'));
                return false;
            }

            $banksresponse = wp_remote_get($this->apiURL . '/banks');

            $banks = wp_remote_retrieve_body($banksresponse);
            $banksArray = [];
            foreach(json_decode($banks) as $bank){
                $banksArray[$bank->id] = array(
                    'nonerefundable' => isset($bank->receive_only)? $bank->receive_only : false,
                    'name' => $bank->name
                );
            }
            $banksArray = array_filter($banksArray);

            if (isset($banksArray[$transaction->transaction->merchant_account->bank_id]) && $banksArray[$transaction->transaction->merchant_account->bank_id]['nonerefundable'] == true) { 
                $adminnotice = new WC_Admin_Notices();
                $adminnotice->add_custom_notice($banksArray[$transaction->transaction->merchant_account->bank_id]['name'], "<div>".$banksArray[$transaction->transaction->merchant_account->bank_id]['name']." does not support refunds at this time.</div>");
                $adminnotice->output_custom_notices();
                return false;
            }

            $amount =  number_format(floatval($refundAmount) * 100, 0, '', '');
            $currency = $order->get_currency();
            $refund = $this->post_refund_data($order_id, $transaction_id, $amount, $currency, $transaction);
            wc_get_logger()->info('Posted Refund: ' . $refund, array('source' => 'paybybank'));
            $refund = json_decode($refund);

            if ($refund && $refund->result == 1) {
                update_post_meta($order_id, 'refund_auth_url', $refund->url);
                return true;
            }else{
                return new WP_Error('error', __('Refund Failed: Refund data is not correct. Please check details and try again. If issue presist, please contact site admin.', 'woo-paybybank'));
                return false;
            }
        } catch (\Exception $e) {
            wc_get_logger()->info('Some error: ' . $e->getMessage(), array('source' => 'paybybank'));

            return new \WP_Error($e->getCode(), $e->getMessage(), isset($e->data) ? $e->data : '');
        }
        return false;
    }

    public function get_transaction($order_id, $transaction_code)
    {
        $order = wc_get_order($order_id);
        $profile_key = $this->profile_key;
        $secret_key = $this->secret_key;
        $signed_date_header =  date('Y-m-d\TH:i:sP');
        $strToHash = implode('', [$profile_key, $transaction_code, $signed_date_header, $secret_key]);
        $hashed = hash("sha512", $strToHash, true);
        $signature = base64_encode($hashed);

        $args = array(
            'headers' => array(
                'X-Fumo-Sign-Date' => $signed_date_header,
                'X-Fumo-Signature' => $signature
            )
        );

        $response = wp_remote_get($this->apiURL . '/transaction/get/' . $profile_key . '/' . $transaction_code, $args);
        
        if (is_wp_error($response)) {
            wc_get_logger()->info('Refund Failed: ' . json_encode($response), array('source' => 'paybybank'));
            new \WP_Error($response->getCode(), json_encode($response->errors), '');

            return false;
        }

        return wp_remote_retrieve_body($response);
    }

    public function post_refund_data($order_id, $transaction_id, $amount, $currency, $transaction)
    {
        $order = wc_get_order($order_id);

        $account_name = get_post_meta( $order_id, '_account_name', true );
        $account_number = get_post_meta( $order_id, '_account_number', true );
        $account_sortcode = get_post_meta( $order_id, '_account_sortcode', true );
        $bank_id = get_post_meta( $order_id, '_bank_id', true );

        $profileKey = $this->profile_key;
        $secretKey = $this->secret_key;
        $strToHash = implode('', [$transaction_id, $profileKey, $currency, $amount, $secretKey]);
        $hashed = hash("sha512", $strToHash, true);
        $signature = base64_encode($hashed);
        $body = array(
            'profile_key' => $profileKey,
            'amount' => intval($amount),
            'currency' => $currency,
            'transaction_id' => $transaction_id,
            'reference' => 'order_' . $order_id,
            'redirect_url' => $order->get_edit_order_url(),
            'to_account' => array(
                'name' => $transaction->transaction->account_name ?? $account_name,
                'scheme' => $transaction->transaction->account_scheme ?? 'UK.OBIE.SortCodeAccountNumber',
                'number' => $transaction->transaction->account_number ? $transaction->transaction->account_number : ($account_sortcode . $account_number),
                'bank_id' => $transaction->transaction->bank->id ?? $bank_id
            ),
            'signature' => $signature,
            'platform' => 'woocommerce'
        );

        wc_get_logger()->info('Body : ' . json_encode($body), array('source' => 'paybybank'));

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->apiURL . '/transaction/refund/' . $transaction_id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($body),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        wc_get_logger()->info('Refund: ' . json_encode($response), array('source' => 'paybybank'));

        if (is_wp_error($response)) {
            wc_get_logger()->info('Refund Failed: ' . json_encode($response), array('source' => 'paybybank'));

            return false;
        }

        return $response;
    }

    function process_refund_ajax() {
        $order_id = $_POST['order_id']; // Get the order ID from the POST data.
    
        $order = wc_get_order($order_id); // Get the order.
    
        if ($order) {
            $result = $order->refund(); // Process the refund.
    
            if ($result) {
                // If the refund is successful, send a successful response.
                wp_send_json_success();
            } else {
                // If the refund fails, send an error response.
                wp_send_json_error('Refund failed.');
            }
        } else {
            // If the order doesn't exist, send an error response.
            wp_send_json_error('Order does not exist.');
        }
    }

    function redirect_to_auth_refund($refund_id, $args) : void {
        set_transient('redirect_after_refund', $args['order_id'], 5);
    }

    function redirect_after_refund_notice() {
        if ($order_id = get_transient('redirect_after_refund')) {
            $redirect_url = get_post_meta($order_id, 'refund_auth_url', true); // Replace with the URL you want to redirect to.
            delete_post_meta($order_id, 'refund_auth_url');
            delete_transient('redirect_after_refund');
    
            echo '<script type="text/javascript">';
            echo 'window.location.href="' . $redirect_url . '";';
            echo '</script>';
        }
    }

    // Add the meta box on the WooCommerce order edit page.
function save_customer_account_details_metabox() {
    global $post;
    $has_error = get_post_meta( $post->ID, '_no_account', true );
    if ( $has_error ) {
        add_meta_box(
            'woocommerce-order-my-custom',
            __( 'Customer Bank Account Details (for refund purpose)' ),
            array($this, 'order_bak_data_meta_box_callback'),
            'shop_order',
            'side',
            'default'
        );
    }
}

// Meta box content.
function order_bak_data_meta_box_callback( $post ) {
    $account_name = get_post_meta( $post->ID, '_account_name', true );
    $account_number = get_post_meta( $post->ID, '_account_number', true );
    $account_sortcode = get_post_meta( $post->ID, '_account_sortcode', true );
    $bank_id = get_post_meta( $post->ID, '_bank_id', true );

    $banksApi = wp_remote_get($this->apiURL . '/banks');
    
    wp_nonce_field( 'save_order_metabox_nonce', 'order_metabox_nonce' );
    ?>
    <p style="border-bottom:solid 1px #eee;padding-bottom:13px;">
        <input type="text" id="account_name" name="account_name" value="<?php echo $account_name; ?>" placeholder="Account Name">
    </p>
    <p style="border-bottom:solid 1px #eee;padding-bottom:13px;">
        <input type="text" id="account_sortcode" name="account_sortcode" value="<?php echo $account_sortcode; ?>" placeholder="Sort Code">
    </p>
    <p style="border-bottom:solid 1px #eee;padding-bottom:13px;">
        <input type="number" id="account_number" name="account_number" value="<?php echo $account_number; ?>" placeholder="Account Number">
    </p>
  
    <p style="border-bottom:solid 1px #eee;padding-bottom:13px;">
        <select name="bank_id" id="bank_id">
            <?php foreach(json_decode($banksApi['body']) as $bank){
                echo '<option '.($bank_id == $bank->id ? 'selected' : '').' value="'.$bank->id.'">' .$bank->name . '</option>';
            } ?>
        </select>
        <!-- <input type="number" id="bank_id" name="bank_id" value="<?php echo $bank_id; ?>" placeholder="Bank ID"> -->
    </p>
    <div>
        <button type="submit" class="button save_order button-primary" name="save" value="Update">Save</button>
    </div>
    <?php
}

// Save the meta box fields.
function save_order_bank_data_metabox( $post_id, $post ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    if ( $post->post_type != 'shop_order' ) return;
    if ( ! isset( $_POST['order_metabox_nonce'] ) || ! wp_verify_nonce( $_POST['order_metabox_nonce'], 'save_order_metabox_nonce' ) ) return;

    update_post_meta( $post_id, '_account_name', $_POST['account_name'] );
    update_post_meta( $post_id, '_account_number', $_POST['account_number'] );
    update_post_meta( $post_id, '_account_sortcode', $_POST['account_sortcode'] );
    update_post_meta( $post_id, '_bank_id', $_POST['bank_id'] );
}

}