<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://studiorav.co.uk
 * @since      1.0.0
 *
 * @package    Woo_PaybyBank
 * @subpackage Woo_PaybyBank/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Woo_PaybyBank
 * @subpackage Woo_PaybyBank/includes
 * @author     Studiorav.co.uk
 */
class Woo_PaybyBank {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Woo_PaybyBank_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'WOO_PAYBYBANK_VERSION' ) ) {
			$this->version = WOO_PAYBYBANK_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'woo-paybybank';

		$this->load_dependencies();

		add_action('plugins_loaded', array($this, 'init_paybybank_gateway'));
		add_filter( 'woocommerce_payment_gateways', array($this, 'paybybank_add_gateway_class') );
	}

	public function init_paybybank_gateway()
	{
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woo-paybybank-actions.php';
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Woo_PaybyBank. Orchestrates the hooks of the plugin.
	 * - Woo_PaybyBank_Admin. Defines all hooks for the admin area.
	 * - Woo_PaybyBank_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the core plugin.
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woo-paybybank-loader.php';

		$this->loader = new Woo_PaybyBank_Loader();
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Woo_PaybyBank    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	public function paybybank_add_gateway_class( $gateways ) {
		$gateways[] = 'WC_PaybyBank_Gateway';
		return $gateways;
	}
}
