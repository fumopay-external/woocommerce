=== Pay by Bank - fumopay ===
Contributors: StudioRav, fumopay
Tags: openbanking, pay by bank
Requires at least: 6.0
Tested up to: 6.1
Requires PHP: 8.0
License: GPL-2.0+

Pay by bank quickly and easily with fumopay integration for your Woocommerce store.

== Description ==
If you have a Woocommerce store and you\'re looking to integrate pay by bank functionality then this plugin will allow you to quickly and easily get started. Simply sign up as a merchant at fumopay, then install our plugin and away you go.

== Installation ==
1. Make sure you are an existing fumopay merchant.
2. Install the plugin and activate it.
3. Copy your test profile key and test secret key from your fumopay dev account.
4. Do a test order and ensure it all works as intended.
5. Add your live profile key and live secret key.
6. Remember to disable test mode.

== Changelog ==
1.2 - Fixed refunds code
1.1 - Added ability to process refunds